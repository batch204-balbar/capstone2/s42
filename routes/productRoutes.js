const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a product
router.post("/", auth.verify, (req,res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin);
	 if (isAdmin) {
		courseController.addProduct(req.body).then(resultFromController => res.send(resultFromController));	 
	} else {
		res.send(false);
	}
	
});

//Route for retrieving all the products
router.get("/Products", (req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));

});

// Route for retrieving all Active Products
router.get("/", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	console.log(req.params)
	productController.getProductId(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
