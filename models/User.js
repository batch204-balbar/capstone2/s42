const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	email:{
		type: String,
		required: [true, "Email address is required"]
	},
	password:{
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: true
	},
	orders: [
	{
		products: {
			productName: String,
			quantity: Number
		},
		totalAmount: Number,
		puchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
})
module.exports = mongoose.model("User", userSchema);