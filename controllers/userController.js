const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Product");

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	});
};

//Controller for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	console.log(newUser);

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	});
};


// User Authentication
module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if(isPasswordCorrect) {     
                return { access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }

    })
}

// Get Profile
module.exports.getProfile = (data) => {
    return User.findById(data.id).then(result => {
        result.password ="";
        return result;
    });
};
