const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create a New Product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	return newProduct.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	});
}


// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Controller for retrieving all Active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving specific product
module.exports.getProductId = (reqParams) => {
	return Product.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Updating a Product
module.exports.updateProduct = (reqParams, reqBody, data) => {
	if(data.isAdmin === true) {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return false
	}
}

