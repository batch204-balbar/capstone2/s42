const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")


const app = express();

//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://angelobalbar:admin123@cluster0.q4we0.mongodb.net/Capstone2?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});

/*
heroku for deployment
 step 1 - subl
new file
filename = Procfile
type sa subl  = web: node index
terminal - 
git add .
git commit -m "Added procfile"
git push origin master
git status
heroku login
then enter (mag open log in to heroko)
terminal = heroku create

copy the link 
https://arcane-spire-68356.herokuapp.com/ | https://git.heroku.com/arcane-spire-
68356.git


then save the link

git remote -v
git push heroku master
--------------------
open postman
----------------
*/